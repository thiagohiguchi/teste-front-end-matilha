import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pick-course',
  templateUrl: './pick-course.component.html',
  styleUrls: ['./pick-course.component.css']
})
export class PickCourseComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  slides = [
    {
      titulo: "Day Programme",
      texto: "Our Day Programmes are designed for families on holiday who would like their children to enjoy a       language course.",
      img: "../assets/img/classroom-yl-i-stock-837483562@2x.jpg",
      titulo2: "Vacation English Plus"
    },
    {
      titulo: "Travelling Classroom",
      texto: "Our Day Programmes are designed for families on holiday who would like their children to enjoy a       language course.",
      img: "../assets/img/giraffe-safari-i-stock-535503242@2x.jpg",
      titulo2: "Vacation English Plus"
    },
    {
      titulo: "Day Programme",
      texto: "Our Day Programmes are designed for families on holiday who would like their children to enjoy a       language course.",
      img: "../assets/img/classroom-yl-i-stock-837483562@2x.jpg",
      titulo2: "Vacation English Plus"
    },
    {
      titulo: "Travelling Classroom",
      texto: "Our Day Programmes are designed for families on holiday who would like their children to enjoy a       language course.",
      img: "../assets/img/giraffe-safari-i-stock-535503242@2x.jpg",
      titulo2: "Vacation English Plus"
    },
    {
      titulo: "Day Programme",
      texto: "Our Day Programmes are designed for families on holiday who would like their children to enjoy a       language course.",
      img: "../assets/img/classroom-yl-i-stock-837483562@2x.jpg",
      titulo2: "Vacation English Plus"
    },
    {
      titulo: "Travelling Classroom",
      texto: "Our Day Programmes are designed for families on holiday who would like their children to enjoy a       language course.",
      img: "../assets/img/giraffe-safari-i-stock-535503242@2x.jpg",
      titulo2: "Vacation English Plus"
    },
    {
      titulo: "Day Programme",
      texto: "Our Day Programmes are designed for families on holiday who would like their children to enjoy a       language course.",
      img: "../assets/img/classroom-yl-i-stock-837483562@2x.jpg",
      titulo2: "Vacation English Plus"
    },
    {
      titulo: "Travelling Classroom",
      texto: "Our Day Programmes are designed for families on holiday who would like their children to enjoy a       language course.",
      img: "../assets/img/giraffe-safari-i-stock-535503242@2x.jpg",
      titulo2: "Vacation English Plus"
    }
  ];

  slideConfig = {
    slidesToShow: 5,
    slidesToScroll: 1,
    dots: false,
    autoplay: true,
    variableWidth: true,
    infinite: true,
    centerMode: false,
    responsive: [{

      breakpoint: 979,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
      }
    }, {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }]
  };

}
