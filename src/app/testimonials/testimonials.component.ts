import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.css']
})
export class TestimonialsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  slides = [
    {
      h4: "a good combination for studying    and having really nice holidays",
      p: "We went on lots of different activities and visited many places but the most beautiful for me were    Xlendi    and    Mgarr ix-Xini as they are also the nearest beaches without any tourists. This small island and the    school    are    a    good combination for studying and having really nice holidays.",
      img: "../assets/img/girl.jpg",
      name: "Sabine Kuestner",
      detail: "IELS Gozo"
    },
    {
      h4: "recommend it to anyone who wants    to learn English",
      p: "The LAL team helped me feel welcome. They were very understanding and I am very happy with both the    school    and my class! I improve my English skills every day and recommend it to anyone who wants to learn    English",
      img: "../assets/img/boy.jpg",
      name: "Mathieu Mongin",
      detail: "General English at LAL Fort Lauderdale"
    },
    {
      h4: "a good combination for studying    and having really nice holidays",
      p: "We went on lots of different activities and visited many places but the most beautiful for me were    Xlendi    and    Mgarr ix-Xini as they are also the nearest beaches without any tourists. This small island and the    school    are    a    good combination for studying and having really nice holidays.",
      img: "../assets/img/girl.jpg",
      name: "Sabine Kuestner",
      detail: "IELS Gozo"
    },
    {
      h4: "recommend it to anyone who wants    to learn English",
      p: "The LAL team helped me feel welcome. They were very understanding and I am very happy with both the    school    and my class! I improve my English skills every day and recommend it to anyone who wants to learn    English",
      img: "../assets/img/boy.jpg",
      name: "Mathieu Mongin",
      detail: "General English at LAL Fort Lauderdale"
    },
    {
      h4: "a good combination for studying    and having really nice holidays",
      p: "We went on lots of different activities and visited many places but the most beautiful for me were    Xlendi    and    Mgarr ix-Xini as they are also the nearest beaches without any tourists. This small island and the    school    are    a    good combination for studying and having really nice holidays.",
      img: "../assets/img/girl.jpg",
      name: "Sabine Kuestner",
      detail: "IELS Gozo"
    },
    {
      h4: "recommend it to anyone who wants    to learn English",
      p: "The LAL team helped me feel welcome. They were very understanding and I am very happy with both the    school    and my class! I improve my English skills every day and recommend it to anyone who wants to learn    English",
      img: "../assets/img/boy.jpg",
      name: "Mathieu Mongin",
      detail: "General English at LAL Fort Lauderdale"
    }
  ];

  slideConfig = {
    slidesToShow: 2,
    slidesToScroll: 2,
    dots: true,
    autoplay: true,
    variableWidth: false,
    responsive: [{

      breakpoint: 650,
      settings: {
        slidesToShow: 1,
        dots: true
      }

    }]
  };



}
