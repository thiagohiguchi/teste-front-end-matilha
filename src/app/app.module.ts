import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { PickCourseComponent } from './pick-course/pick-course.component';

@NgModule({
  declarations: [
    AppComponent,
    TestimonialsComponent,
    PickCourseComponent
  ],
  imports: [
    BrowserModule,
    SlickCarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
